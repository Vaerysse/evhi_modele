using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodDrop : MonoBehaviour
{
    private AudioSource drop;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoopAudio());
    }

    IEnumerator LoopAudio()
    {
        float loopTimer;
        drop = GetComponent<AudioSource>();

        while (true)
        {
            loopTimer = Random.Range(1, 10);
            drop.Play();
            yield return new WaitForSeconds(loopTimer);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
