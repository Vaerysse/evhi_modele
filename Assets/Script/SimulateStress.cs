using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulateStress : MonoBehaviour
{
    public float stressValue = 100.0f;
    private int wait = 1;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ChangeStressValue());
    }

    IEnumerator ChangeStressValue()
    {
        if (GameObject.Find("GameManager").GetComponent<GameManagerSystem>().roomName != "SafeRoom" && GameObject.Find("GameManager").GetComponent<GameManagerSystem>().roomName != "GameOverRoom")
        {
            float chanceToPanic;
            float chanceNoPanic;

            while (true)
            {
                chanceToPanic = Random.Range(0, 100);
                chanceNoPanic = Random.Range(0, 100);

                if (chanceToPanic < 2 * GameObject.Find("GameManager").GetComponent<GameManagerSystem>().nbRoomVisited && stressValue <= 220.0f)
                {
                    stressValue += 10.0f;
                    GameObject.Find("GameManager").GetComponent<GameManagerSystem>().stress = stressValue;
                }
                else
                {
                    if(chanceNoPanic <= 10)
                    {
                        stressValue -= 2.0f;
                        GameObject.Find("GameManager").GetComponent<GameManagerSystem>().stress = stressValue;
                    }
                    else if(chanceNoPanic > 10 && stressValue <= 220.0f)
                    {
                        stressValue += 1.0f;
                        GameObject.Find("GameManager").GetComponent<GameManagerSystem>().stress = stressValue;
                    }

                }
                yield return new WaitForSeconds(wait);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
