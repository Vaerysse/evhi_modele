using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class DataIOSystem : MonoBehaviour
{
    private string path; // Chemin d�acc�s � la matrice utilisateur pr�sent dans le syst�me

    public GameObject prefabFear; // Prefab de Fear pour le chargement du fichier

    public int nbModel = 1; // Permet de sauvegarder independament chaque mise � jours du model

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Met � jour la matrice utilisateur pr�sent dans le syst�me
    public void saveData(List<GameObject> listFear)
    {
        string name = "";
        string int1 = "";
        string int2 = "";
        string int3 = "";
        string fail = "";
        string win = "";
        int cpt = 1;


        foreach (GameObject go_f in listFear)
        {
            name = name + go_f.GetComponent<FearSystem>().getFearName();
            int1 = int1 + go_f.GetComponent<FearSystem>().getIntensity()[0];
            int2 = int2 + go_f.GetComponent<FearSystem>().getIntensity()[1];
            int3 = int3 + go_f.GetComponent<FearSystem>().getIntensity()[2];
            fail = fail + go_f.GetComponent<FearSystem>().getFail();
            win = win + go_f.GetComponent<FearSystem>().getWin();

            if (cpt != listFear.Count)
            {
                name = name + ";";
                int1 = int1 + ";";
                int2 = int2 + ";";
                int3 = int3 + ";";
                fail = fail + ";";
                win = win + ";";
            }
            cpt++;

        }

        try
        {
            StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/LearnerModel.csv");
            //StreamWriter writer = new StreamWriter("E:/Unity_project/evhi_modele/Assets/User/LearnerModel" + nbModel + ".csv");

            writer.WriteLine(name);
            writer.WriteLine(int1);
            writer.WriteLine(int2);
            writer.WriteLine(int3);
            writer.WriteLine(fail);
            writer.WriteLine(win);

            writer.Close();

            nbModel++;
        }
        catch
        {
            Debug.Log("Probl�me lors de la sauvegarde du model!");
        }
    }

    // Charge la matrice de la mod�lisation utilisateur
    public List<GameObject> loadData()
    {
        List<GameObject> UserModel = new List<GameObject>();
        StreamReader reader = null;
        
        // On test si un fichier existe d�j�
        try
        {
            Debug.Log(Application.persistentDataPath);
            reader = new StreamReader(Application.persistentDataPath + "/LearnerModel.csv");
            bool endOfFile = false;
            int nbLine = 1;
            while (!endOfFile)
            {
                string data_string = reader.ReadLine();
                if (data_string == null)
                {
                    endOfFile = true;
                    break;
                }
                var data = data_string.Split(";");

                if (nbLine == 1)
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        UserModel.Add(Instantiate(prefabFear));
                    }
                    int indice = 0;
                    foreach (GameObject go_f in UserModel)
                    {
                        go_f.GetComponent<FearSystem>().setName(data[indice]);
                        indice++;
                    }
                }
                else if (nbLine == 2)
                {
                    int indice = 0;
                    foreach (GameObject go_f in UserModel)
                    {
                        go_f.GetComponent<FearSystem>().majIntensity(new List<float> { float.Parse(data[indice]) });
                        indice++;
                    }
                }
                else if (nbLine == 3)
                {
                    int indice = 0;
                    foreach (GameObject go_f in UserModel)
                    {
                        go_f.GetComponent<FearSystem>().majIntensity(new List<float> { go_f.GetComponent<FearSystem>().getIntensity()[0], float.Parse(data[indice]) });
                        indice++;
                    }
                }
                else if (nbLine == 4)
                {
                    int indice = 0;
                    foreach (GameObject go_f in UserModel)
                    {
                        go_f.GetComponent<FearSystem>().majIntensity(new List<float> { go_f.GetComponent<FearSystem>().getIntensity()[0], go_f.GetComponent<FearSystem>().getIntensity()[1], float.Parse(data[indice]) });
                        go_f.GetComponent<FearSystem>().initialisation(); // Une fois le chargement fini on le signal
                        indice++;
                    }
                }

                nbLine++;
            }
        }
        catch// sinon on en cr�er un factuelle 
        {
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));
            UserModel.Add(Instantiate(prefabFear));

            UserModel[0].GetComponent<FearSystem>().setName("Arachnophobia1");
            UserModel[1].GetComponent<FearSystem>().setName("Arachnophobia2");
            UserModel[2].GetComponent<FearSystem>().setName("Arachnophobia3");
            UserModel[3].GetComponent<FearSystem>().setName("Ophiophobia1");
            UserModel[4].GetComponent<FearSystem>().setName("Ophiophobia2");
            UserModel[5].GetComponent<FearSystem>().setName("Ophiophobia3");
            UserModel[6].GetComponent<FearSystem>().setName("Acrophobia");
            UserModel[7].GetComponent<FearSystem>().setName("Achluophobia");
            UserModel[8].GetComponent<FearSystem>().setName("Hematophobia");
            UserModel[9].GetComponent<FearSystem>().setName("Claustrophobia");

            UserModel[0].GetComponent<FearSystem>().majIntensity(new List<float> { 0.1f, 0.0f, 0.0f });
            UserModel[1].GetComponent<FearSystem>().majIntensity(new List<float> { 0.2f, 0.0f, 0.0f });
            UserModel[2].GetComponent<FearSystem>().majIntensity(new List<float> { 0.2f, 0.5f, 0.0f });
            UserModel[3].GetComponent<FearSystem>().majIntensity(new List<float> { 0.3f, 0.0f, 0.0f });
            UserModel[4].GetComponent<FearSystem>().majIntensity(new List<float> { 0.1f, 0.2f, 0.1f });
            UserModel[5].GetComponent<FearSystem>().majIntensity(new List<float> { 0.2f, 0.5f, 0.0f });
            UserModel[6].GetComponent<FearSystem>().majIntensity(new List<float> { 0.2f, 0.2f, 0.3f });
            UserModel[7].GetComponent<FearSystem>().majIntensity(new List<float> { 0.2f, 0.4f, 0.0f });
            UserModel[8].GetComponent<FearSystem>().majIntensity(new List<float> { 0.1f, 0.3f, 0.5f });
            UserModel[9].GetComponent<FearSystem>().majIntensity(new List<float> { 0.25f, 0.0f, 0.0f });

            foreach (GameObject go_f in UserModel)
            {
                go_f.GetComponent<FearSystem>().initialisation();
            }

            saveData(UserModel);
        }

        return UserModel;
    }
}
