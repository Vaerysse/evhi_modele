using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalsFearManager : MonoBehaviour
{

    public int level = 1;
    public bool spider = true;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void initialisation(int levelLoad, bool spiderLoad)
    {
        level = levelLoad;
        spider = spiderLoad;
        //Affichage des bon �l�ment pour la sc�ne
        GameObject[] GOToUnspawn;

        if (spider)
        {
            // Hide Snake GO
            GOToUnspawn = GameObject.FindGameObjectsWithTag("Snake1");
            foreach (GameObject go in GOToUnspawn)
                go.SetActive(false);

            GOToUnspawn = GameObject.FindGameObjectsWithTag("Snake2");
            foreach (GameObject go in GOToUnspawn)
                go.SetActive(false);

            GOToUnspawn = GameObject.FindGameObjectsWithTag("Snake3");
            foreach (GameObject go in GOToUnspawn)
                go.SetActive(false);

            switch (level)
            {
                case 1:
                    GOToUnspawn = GameObject.FindGameObjectsWithTag("Spider2");
                    foreach (GameObject go in GOToUnspawn)
                        go.SetActive(false);

                    GOToUnspawn = GameObject.FindGameObjectsWithTag("Spider3");
                    foreach (GameObject go in GOToUnspawn)
                        go.SetActive(false);

                    break;

                case 2:
                    GOToUnspawn = GameObject.FindGameObjectsWithTag("Spider3");
                    foreach (GameObject go in GOToUnspawn)
                        go.SetActive(false);

                    break;
            }
        }
        else
        {
            // Hide Spider GO
            GOToUnspawn = GameObject.FindGameObjectsWithTag("Spider1");
            foreach (GameObject go in GOToUnspawn)
                go.SetActive(false);

            GOToUnspawn = GameObject.FindGameObjectsWithTag("Spider2");
            foreach (GameObject go in GOToUnspawn)
                go.SetActive(false);

            GOToUnspawn = GameObject.FindGameObjectsWithTag("Spider3");
            foreach (GameObject go in GOToUnspawn)
                go.SetActive(false);

            switch (level)
            {

                case 1:
                    GOToUnspawn = GameObject.FindGameObjectsWithTag("Snake2");
                    foreach (GameObject go in GOToUnspawn)
                        go.SetActive(false);

                    GOToUnspawn = GameObject.FindGameObjectsWithTag("Snake3");
                    foreach (GameObject go in GOToUnspawn)
                        go.SetActive(false);
                    break;

                case 2:
                    GOToUnspawn = GameObject.FindGameObjectsWithTag("Snake3");
                    foreach (GameObject go in GOToUnspawn)
                        go.SetActive(false);
                    break;
            }
        }
    }
}
