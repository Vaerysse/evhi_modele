using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestSounds : MonoBehaviour
{
    private AudioSource source;
    public AudioClip wolf;
    public AudioClip walk;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
        StartCoroutine(WolfAudio());

    }
    
    IEnumerator WolfAudio()
    {
        float loopTimer;
        int chooseSound;

        while (true)
        {
            chooseSound = (int) Random.Range(1, 2.9f);
            Debug.Log(chooseSound);

            switch(chooseSound)
            {
                case 1:
                    source.PlayOneShot(wolf);
                    break;
                case 2:
                    source.PlayOneShot(walk);
                    break;
            }

            loopTimer = Random.Range(10, 30);
            yield return new WaitForSeconds(loopTimer);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
