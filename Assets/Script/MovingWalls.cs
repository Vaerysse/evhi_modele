using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWalls : MonoBehaviour
{
    public GameObject wall1;
    public GameObject wall2;
    public GameObject wall3;
    public GameObject wall4;
    public GameObject ceiling;

    public float speed = 0.1f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        wall1.transform.position = Vector3.MoveTowards(wall1.transform.position, new Vector3(wall1.transform.position.x, wall1.transform.position.y, 0), speed * Time.deltaTime);
        wall2.transform.position = Vector3.MoveTowards(wall2.transform.position, new Vector3(0, wall2.transform.position.y, wall2.transform.position.z), speed * Time.deltaTime);
        wall3.transform.position = Vector3.MoveTowards(wall3.transform.position, new Vector3(wall3.transform.position.x, wall3.transform.position.y, 0), speed * Time.deltaTime);
        wall4.transform.position = Vector3.MoveTowards(wall4.transform.position, new Vector3(0, wall4.transform.position.y, wall4.transform.position.z), speed * Time.deltaTime);
        ceiling.transform.position = Vector3.MoveTowards(ceiling.transform.position, new Vector3(ceiling.transform.position.x, 0, ceiling.transform.position.y), (speed * Time.deltaTime)/2);
    }
}
