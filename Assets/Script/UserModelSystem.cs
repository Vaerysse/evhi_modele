using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserModelSystem : MonoBehaviour
{
    public List<GameObject> fearModel;

    private bool initOk = false;

    // Start is called before the first frame update
    void Start()
    {
        if (!initOk)
        {
            // On emp�che le game object d'�tre d�truit  entre les sc�nes
            DontDestroyOnLoad(this);
            initOk = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Initialise le model selon la liste de peur re�u
    public void initialisation(List<GameObject> listFear)
    {
        fearModel = listFear;
    }

    // Renvoie la mod�lisation de l�utilisateur pour une phobie sp�cifique
    public List<float> fearStatut(string name)
    {
        // On parcours la liste des peurs
        foreach(GameObject go_f in fearModel)
        {
            // Si le nom correspond au nom re�ue
            if(go_f.GetComponent<FearSystem>().getFearName() == name)
            {
                return go_f.GetComponent<FearSystem>().getIntensity();
            }
        }
        // si on a pas trouver la phobie du nom re�ue on renvoie une list vide
        return new List<float>();
    }

    // Met � jour la mod�lisation de l�utilisateur pour une des phobies
    public void majFearStatut(List<float> intensity, string name)
    {
        //Parcourt la liste des phobies et met � jour la mod�lisation de cette phobie par rapport aux donn�es re�ues
        List<float> intens = intensity;
        if(intensity.Count > 0 && name != "safe room") 
        {
            intens.Remove(intens.Count - 1);
            foreach (GameObject go_f in fearModel)
            {
                if (go_f.GetComponent<FearSystem>().getFearName() == name)
                {
                    // On r�cup�re la mod�lisation ectuel de la peur
                    List<float> currentFearInt = go_f.GetComponent<FearSystem>().getIntensity();
                    // On parcours cette liste pour la mettre � jours avec les nouvelle donn�es
                    for (int i = 0; i < currentFearInt.Count; i++)
                    {
                        currentFearInt[i] = (currentFearInt[i] * 0.5f) + (intens[i] * 0.5f);
                    }
                    // On met � jour
                    go_f.GetComponent<FearSystem>().majIntensity(currentFearInt);
                    if (intensity[intensity.Count - 1] == 0.0f)
                    {
                        go_f.GetComponent<FearSystem>().addFail();
                    }
                    else
                    {
                        go_f.GetComponent<FearSystem>().addWin();
                    }
                }
            }
        }
    }

    // Renvoie la mod�lisation de l'apprenant
    public List<GameObject> modelisation()
    {
        return fearModel;
    }

    // Renvoie les phobies dont l�intensit� pour l�utilisateur sera de plus en plus �lev�e au fur et � mesure du parcours des salles
    public List<GameObject> listFearPerIntensity(int numRoom)
    {
        int numPar = numRoom;
        if(numPar > 9)
        {
            numPar = 9;
        }
        List<GameObject> selecFear = new List<GameObject>();

        while(selecFear.Count == 0)
        {
            // Si on est sur le parametre la plus bas, on prend les peur dont l'intensit� sont la moins �lev�
            if(numPar == 0)
            {
                float minInt = 1.0f;

                // On parcours la liste des phobie vois celles qui ont le moins d'intensit�
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (totIntensity <= minInt)
                    {
                        minInt = totIntensity;
                    }
                }

                // On reparcours la liste des phobie pour ajout� � la liste les phobie correspondant � la plus basse intensit�
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (totIntensity == minInt)
                    {
                        selecFear.Add(go_f);
                    }
                }
            } // Premier niveau : Max de peur 10%
            else if(numPar == 1)
            {
                foreach(GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if(totIntensity <= 0.1f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Deuxi�me niveau : Max de peur 20%
            else if (numPar == 2)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (totIntensity <= 0.2f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Troisi�me niveau : MIN de peur 10% et Max de peur 30%
            else if (numPar == 3)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (totIntensity >= 0.1f && totIntensity <= 0.3f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Quatri�me niveau : Max de peur 40% et au moins 10% en intensit� 2
            else if (numPar == 4)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (go_f.GetComponent<FearSystem>().getIntensity()[1] >= 0.1f && totIntensity <= 0.4f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            } // Cinqui�me niveau : MAX de peur 60% et au moins 20% en intensit� 2
            else if (numPar == 5)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (go_f.GetComponent<FearSystem>().getIntensity()[1] >= 0.2f && totIntensity <= 0.6f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Sixi�me niveau : MAX de peur 70% et au moins 40% en intensit� 2
            else if (numPar == 6)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (go_f.GetComponent<FearSystem>().getIntensity()[1] >= 0.4f && totIntensity <= 0.7f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Septi�me niveau : MAX de peur 80% et au moins 10% en intensit� 3
            else if (numPar == 7)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (go_f.GetComponent<FearSystem>().getIntensity()[2] >= 0.1f && totIntensity <= 0.8f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Huiti�me niveau : MAX de peur 90% et au moins 20% en intensit� 3
            else if (numPar == 8)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (go_f.GetComponent<FearSystem>().getIntensity()[2] >= 0.2f && totIntensity <= 0.9f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }// Neuvi�me niveau : MAX de peur 90% et au moins 30% en intensit� 3
            else if (numPar == 9)
            {
                foreach (GameObject go_f in fearModel)
                {
                    float totIntensity = go_f.GetComponent<FearSystem>().totIntensity();
                    if (go_f.GetComponent<FearSystem>().getIntensity()[2] >= 0.3f && totIntensity <= 0.9f)
                    {
                        selecFear.Add(go_f);
                    }
                }
            }

            // Si on a pas de peur au numPar selectionn�, on diminue d'un niveau
            // On diminue et on boucle jusqu'a avoir au moins une peur de selectionn�
            if(selecFear.Count == 0)
            {
                numPar = numPar - 1;
            }
        }

        return selecFear;
    }

}
