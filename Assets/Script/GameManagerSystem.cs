using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerSystem : MonoBehaviour
{
    public int nbRoomVisited; // Nombre de pi�ces visit�es par l'utilisateur depuis le d�but de l'�cp�rience
    private List<float> intensityProgress; // Historique des intensit�s pour chaque niveau parcouru. Achaque nouveau niveau l'intensit� choisie pour charger le niveau est stock�e
    //public GameObject Sensor;
    private List<float> dataSensor; // Mis � jour � chaque update de Unity avec les valeurs relev�es des capteurs.Premi�re position le rythme cardiaque, deuxi�me position la mesure �lectrodermale
    private List<float> stressLevelTime; // Temps pass� dans chaque niveau de stress pour la salle de phobie en cours d�exp�rience(4 niveaux : entr�e 1 -> pas de peur, entr�e 2 -> niveau 1, entr�e 3 -> niveau 2 et entr�e 4 -> niveau 3)
    public float stress; // Mis � jour � chaque update, permet de conna�tre l��tat de stress de l�utilisateur.R�initialis� � 0 lors de l�entr�e dans une nouvelle pi�ce par l�utilisateur
    public float timeBeforeEndGame; // Compteur initialis� � 30 qui se d�compte au court du temps lorsque l�utilisateur arrive en stress d�intensit� 3
    private List<float> stressLevelLimit; // Quel stress (en fonction des param�tres de d�part de l�utilisateur) va d�clencher les 3 niveaux d�intensit� de stress
    public string roomName; // Nom de la phobie de la salle en cours d'exp�rience
    private GameObject UserCurrent; // Contient la classe User qui mod�lise l�utilisateur actuel
    private float timeSensor; // L�heure de la derni�re mise � jour du calcul du stress
    public bool gameOver; // Permet de v�rifier si l�utilisateur est en game over
    public bool exitRoom; // Passe � True quand l�utilisateur arrive � sortir de la pi�ce o� il se trouve.Repasse � False lors de l�entr�e dans une nouvelle pi�ce par l�utilisateur
    private bool initOK = false; // Cette variable permet d'�viter d'initialis� � chaque chargement toutes les variable (certain on besoin d'�tre suivit tous le long du jeu)
    private bool loadNewSceneOk = false;
    private bool safeRoomOk = true; // signal que la safe room � �t� charg�
    private int intensityCurrentStress = 1; // Niveau de stress actuelle

    // Pra pour la salle animal fear
    private int animalFearLevel = 1;
    private bool animalFearSpider = true;


    public GameObject preFabLearner; // Prefab pour la modelisation de l'apprenant

    // Pour l'IHM du joueur
    private GameObject debugConsole;
    private GameObject alerteInfo;
    private GameObject stressInfo;

    //Pour la transition entre le  mode vr et le mode PC
    public bool vrMode = true;
    public bool captorMode = false;


    // Start is called before the first frame update
    void Start()
    {
        initialisation();
    }

    // Update is called once per frame
    void Update()
    {

        if (loadNewSceneOk)
        {
            loadNewSceneOk = false;
            initNewLevel();
        }
        // On met � jour les diff�rentes variables des capteurs et suivis de progression
        //dataSensor = Sensor.GetComponent<SensorManager>().updateDataSensor();

        //debugConsole.GetComponent<Text>().text = "Sensor cardiaque : " + dataSensor[0] + ", sensor autre : " + dataSensor[1];

        calculeStressLevel();

        // Si la sortie a �t� atteinte et que l�utilisateur n��tait pas dans une safe room on mod�lise l�utilisateur pour la salle qu�il vient de parcourir
        if (exitRoom)
        {
            //debugConsole.GetComponent<Text>().text = debugConsole.GetComponent<Text>().text + "/n Fin de la zone";
            List<float> model = modelUserForRoom();
            // On pr�cise si l'utilisateur � eu un game over ou non
            if (gameOver)
            {
                model.Add(0.0f);
            }
            else
            {
                model.Add(1.0f);
            }
            // Puis on envoie le mod�le � User pour une mise � jour du mod�le de l�utilisateur
            UserCurrent.GetComponent<UserModelSystem>().majFearStatut(model, roomName);
            // On enregistre la nouvelle mod�lisation
            saveProfil();

            // On charge ensuite une nouvelle sc�ne
            if (gameOver)
            {
                endGame();
            }
            else
            {
                loadNewRoom();
            }
            exitRoom = false;
        }

        // Si on passe en stress de niveau 3 on d�clanche le compteur pour savoir si cela dure plus de 30s
        if(stress > stressLevelLimit[2])
        {
            timeBeforeEndGame = timeBeforeEndGame - Time.deltaTime;

            // Si cela fait plus de 30 seconde, on d�clanche le game over
            if(timeBeforeEndGame < 0)
            {
                exitRoom = true;
                gameOver = true;
            }
        }
        else if(stress <= stressLevelLimit[2] && timeBeforeEndGame != 30.0f) // si on est pas en intensit� de peur 3 on r�initialise le compteur si ce n'a pas �t� fait
        {
            timeBeforeEndGame = 30.0f;
        }
    }


    // Initialise les variable � chaque chargement
    // Certaine variable ne sont initialis� que la toute premi�re fois
    private void initialisation()
    {
        if (!initOK)
        {
            nbRoomVisited = 1;
            intensityProgress = new List<float> { 0.0f };
            dataSensor = new List<float> {0.0f, 0.0f};
            stressLevelTime = new List<float> { 0.0f, 0.0f, 0.0f, 0.0f };
            stress = 0.0f;
            timeBeforeEndGame = 30.0f;
            stressLevelLimit = new List<float> { 120.0f, 150.0f, 180.0f }; // A DEFINIR
            roomName = "Start Game";
            UserCurrent = Instantiate(preFabLearner);
            timeSensor = 0.0f;
            gameOver = false;
            exitRoom = false;
            initOK = true;
            // On charge et cr�er le profil utilisateur
            loadProfil();
            initNewLevel();
            // On emp�che le game object d'�tre d�truit  entre les sc�nes
            DontDestroyOnLoad(this);
        }
        else
        {
            initNewLevel();
        }
    }

    // Initialise toutes les variables dont on a besoin lors d'un chargement d'un nouveau niveau
    public void initNewLevel()
    {
        dataSensor[0] = 0.0f;
        dataSensor[1] = 0.0f;
        
        stressLevelTime[0] = 0.0f;
        stressLevelTime[1] = 0.0f;
        stressLevelTime[2] = 0.0f;
        stressLevelTime[3] = 0.0f;
        
        //Pour les test
        stress = 0.0f;
        timeBeforeEndGame = 30.0f;
        timeSensor = 0.0f;
        exitRoom = false;

        // Si c'est la salle animalFear il faut changer ses parametres
        if(roomName == "Arachnophobia1" || roomName == "Arachnophobia2" || roomName == "Arachnophobia3" || roomName == "Ophiophobia1" || roomName == "Ophiophobia2" || roomName == "Ophiophobia3")
        {
            GameObject roomMana = GameObject.Find("RoomManager");
            roomMana.GetComponent<AnimalsFearManager>().initialisation(animalFearLevel, animalFearSpider);
        }

        //Si c'est le Game Over alors on r�initialise le nombre de salle travers�
        if(roomName == "GameOverRoom")
        {
            nbRoomVisited = 1;
            gameOver = false;
        }

        if (captorMode)
        {
            GetComponent<SimulateStress>().enabled = false;
        }
        else
        {
            GetComponent<SimulateStress>().stressValue = 100.0f;
        }

        // En mode VR on utilise le panel situ� sur le poign� du joueur
        if (vrMode)
        {
            debugConsole = GameObject.FindGameObjectsWithTag("PanelInfo")[0];
            alerteInfo = GameObject.FindGameObjectsWithTag("PanelAlerte")[0];
            stressInfo = GameObject.FindGameObjectsWithTag("PanelStress")[0];
            GameObject.Find("FirstPersonController").SetActive(false);
        }
        else
        {
            GameObject.Find("OVRPlayerControllerVariant").SetActive(false);
        }
    }

    //Charge une nouvelle pi�ce selon la mod�lisation de l�utilisateur et l��tat de l�avancement de l�exp�rience
    private void loadNewRoom()
    {
        exitRoom = false;

        // On determine si la nouvelle salle doit �tre une safe room
        if (nbRoomVisited % 3 == 0 && safeRoomOk)
        {
            roomName = "SafeRoom";
            safeRoomOk = false;
            loadLevel(roomName);
        }
        else // Sinon on determine quelle est la prochaine intensit� de peur max
        {
            safeRoomOk = true;
            intensityProgress.Add(intensityProgress[intensityProgress.Count - 1] + 0.1f);
            // On demande la liste des peurs associ�es au nombre de pi�ce d�j� travers�
            List<GameObject> listFear = UserCurrent.GetComponent<UserModelSystem>().listFearPerIntensity(nbRoomVisited);
            // On choisit une phobie au hasard et on envoie une demande pour charger une nouvelle sc�ne
            int r = Random.Range(0,listFear.Count);
            roomName = listFear[r].GetComponent<FearSystem>().getFearName();
            nbRoomVisited = nbRoomVisited + 1; // Une pi�ce en plus de visit�
            loadLevel(roomName);
        }
    }

    //Calcule le stress de l�utilisateur � l�instant T
    private void calculeStressLevel()
    {
        // On r�cup�re les donn�es des capteurs
        //List<float> dataSensor = SensorManager.updateDataSensor();
        //stress = (dataSensor[0] * 0.5f) + (dataSensor[1] * 0.5f);
        // On met ensuite � jour le suivi du stress et le display
        intensityCurrentStress = majStressLevelTime();
        if (vrMode)
        {
            MAJDisplay(intensityCurrentStress);
        }
    }

    //Met � jour le suivi de temps de stress pass� dans chaque niveau de stress pour la salle en cours d�exp�rience, retourne le niveau de stress � afficher � l�utilisateur
    private int majStressLevelTime()
    {
        if(stress < stressLevelLimit[0])
        {
            stressLevelTime[0] = stressLevelTime[0] + Time.deltaTime;
            return 1;
        }
        else if(stress >= stressLevelLimit[0] && stress < stressLevelLimit[1])
        {
            stressLevelTime[1] = stressLevelTime[1] + Time.deltaTime;
            return 2;
        }
        else if (stress >= stressLevelLimit[1] && stress < stressLevelLimit[2])
        {
            stressLevelTime[2] = stressLevelTime[2] + Time.deltaTime;
            return 3;
        }
        else
        {
            stressLevelTime[3] = stressLevelTime[3] + Time.deltaTime;
            return 4;
        }
    }

    // R�cup�re la mod�lisation utilisateur et l�envoie � DataIO
    private void saveProfil()
    {
        // On demande � User toutes ses Fear et on les envoie � DataIO qui s�occupera de la sauvegarde
        GetComponent<DataIOSystem>().saveData(UserCurrent.GetComponent<UserModelSystem>().modelisation());
    }

    // Cr�e un User selon les donn�es charg�es par DataIO
    private void loadProfil()
    {
        List<GameObject> listFear;

        // On charge l mod�lisation de l'utilisateur
        listFear = GetComponent<DataIOSystem>().loadData();
        // Puis on cr�e le User (la mod�lisation de l�utilisateur)
        UserCurrent.GetComponent<UserModelSystem>().initialisation(listFear);
    }

    // Cr�e une mod�lisation de l�utilisateur selon son exp�rience pass�e lors de la salle actuelle de l�exp�rience
    private List<float> modelUserForRoom()
    {
        // On transforme le temps pass� dans chaque niveau de stress en pourcentage
        float totalTime = 0.0f;
        foreach(float time in stressLevelTime)
        {
            totalTime = totalTime + time;
        }
        List<float> model = new List<float>();
        model.Add(stressLevelTime[0] / totalTime);
        model.Add(stressLevelTime[1] / totalTime);
        model.Add(stressLevelTime[2] / totalTime);
        model.Add(stressLevelTime[3] / totalTime);
        return model;
    }

    // Donne quel object � afficher � l'interface utilisateur selon le niveau de stress
    private void MAJDisplay(int lvlStress)
    {
        if(roomName == "Start Game")
        {
            debugConsole.GetComponent<Text>().text = "Dans chaque pi�ce tu devras \n trouver une clef pour t'�chapper !";
        }
        else if(roomName == "SafeRoom"){
            debugConsole.GetComponent<Text>().text = "Ici tu peux souffler, prend ton temps. \n Une fois repos� prend la clef et passe la porte !";
        }
        else if(roomName == "Acrophobia")
        {
            debugConsole.GetComponent<Text>().text = "Traverse la planche pour prendre la clef et sortir. \n Courage !";
        }
        else if(roomName == "Hematophobia")
        {
            debugConsole.GetComponent<Text>().text = "Berk tout ce sang! \n Trouve la clef et �chappe toi!";
        }
        else if(roomName == "Claustrophobia")
        {
            debugConsole.GetComponent<Text>().text = "Respire et d�tend toi ... \n Trouve la bonne clef pour sortir!";
        }
        else if (roomName == "Achluophobia")
        {
            debugConsole.GetComponent<Text>().text = "Suis les lumi�res pour trouver la clef et la sortie !";
        }
        else if (roomName == "GameOverRoom")
        {
            debugConsole.GetComponent<Text>().text = "Game Over T.T";
        }
        else
        {
            debugConsole.GetComponent<Text>().text = "Trouve la clef et �chappe toi !";
        }

        if(intensityCurrentStress == 1)
        {
            alerteInfo.GetComponent<Text>().text = "";
            stressInfo.GetComponent<Text>().color = Color.white;
        }
        else if(intensityCurrentStress == 2)
        {
            alerteInfo.GetComponent<Text>().text = "";
            stressInfo.GetComponent<Text>().color = Color.green;
        }
        else if(intensityCurrentStress == 3)
        {
            alerteInfo.GetComponent<Text>().text = "";
            stressInfo.GetComponent<Text>().color = Color.yellow;
        }
        else
        {
            alerteInfo.GetComponent<Text>().text = "Game over dans : " + timeBeforeEndGame;
            stressInfo.GetComponent<Text>().color = Color.red;
        }
        stressInfo.GetComponent<Text>().text = "Stress info : " + stress;
    }

    // Permet d'afficher la bonne salle selon le nom de la salle retenue
    private void loadLevel(string roomNameLoad)
    {
        roomName = roomNameLoad;

        if (roomNameLoad == "Arachnophobia1")
        {
            roomNameLoad = "AnimalsFear";
            animalFearLevel = 1;
            animalFearSpider = true;
        }
        else if (roomNameLoad == "Arachnophobia2")
        {
            roomNameLoad = "AnimalsFear";
            animalFearLevel = 2;
            animalFearSpider = true;
        }
        else if (roomNameLoad == "Arachnophobia3")
        {
            roomNameLoad = "AnimalsFear";
            animalFearLevel = 3;
            animalFearSpider = true;
        }
        else if (roomNameLoad == "Ophiophobia1")
        {
            roomNameLoad = "AnimalsFear";
            animalFearLevel = 1;
            animalFearSpider = false;
        }
        else if (roomNameLoad == "Ophiophobia2")
        {
            roomNameLoad = "AnimalsFear";
            animalFearLevel = 2;
            animalFearSpider = false;
        }
        else if (roomNameLoad == "Ophiophobia3")
        {
            roomNameLoad = "AnimalsFear";
            animalFearLevel = 3;
            animalFearSpider = false;
        }

        loadNewSceneOk = true;

        SceneManager.LoadScene(roomNameLoad);
    }

    // G�re le game over
    private void endGame()
    {

        if (!vrMode)
        {
            saveProfil();
        }

        loadNewSceneOk = true;
        roomName = "GameOverRoom";
        SceneManager.LoadScene(roomName);
    }

}
