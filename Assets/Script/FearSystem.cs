using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FearSystem : MonoBehaviour
{
    public string fearName; //  Nom de la phobie.
    public List<float> intensity = new List<float>(); //  Donn�es de la mod�lisation utilisateur pour cette phobie. C�est le pourcentage de temps pass� dans les diff�rents niveaux de stress.
    public float nbFail = 0.0f; //  Nombre de fois o� l�utilisateur a �chou� contre cette phobie.
    public float nbWin = 0.0f; //  Nombre de fois o� l�utilisateur a r�ussi contre cette phobie.

    private bool initOk = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Initialisation de la phobie
    public void initialisation()
    {
        if (!initOk) {
            initOk = true;
            // On emp�che le game object d'�tre d�truit  entre les sc�nes
            DontDestroyOnLoad(this);
        }
    }

    // Renvoie le nom de la phobie
    public string getFearName()
    {
        return fearName;
    }

    // Modifie le nom de la peur
    public void setName(string newName)
    {
        fearName = newName;
    }

    // Renvoie les valeurs des 3 niveaux d'intensit�s
    public List<float> getIntensity()
    {
        return intensity;
    }

    // Renvoie le nombre de fois ou l'utilisateur � surmont� cette peur
    public float getWin()
    {
        return nbWin;
    }

    // Renvoie le nombre de fois ou l'utilisateur n'a pas surmont� cette peur
    public float getFail()
    {
        return nbFail;
    }

    public void majIntensity(List<float> newIntensity)
    {
        intensity = newIntensity;
    }

    // Imcr�mente le nombre de win
    public void addWin()
    {
        nbWin = nbWin + 1.0f;
    }

    // Incr�mente le nombre de fail
    public void addFail()
    {
        nbFail = nbFail + 1.0f;
    }

    // return le total des trois niveaux d'intensit� de la mod�lisation de cette phobie
    public float totIntensity()
    {
        float totInten = 0.0f;
        foreach(float inten in intensity){
            totInten = totInten + inten;
        }
        return totInten;
    }
}
