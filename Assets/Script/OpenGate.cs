using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenGate : MonoBehaviour
{
    public bool closeDoor;
    private float cpt = 20.0f;

    // Start is called before the first frame update
    void Start()
    {
        closeDoor = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Si la clef entre en contacte avec la porte on passe dans le niveau suivant
    // Sinon on signal qu'il faut une clef
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Key") && closeDoor)
        {
            closeDoor = false;
            GameObject.Find("GameManager").GetComponent<GameManagerSystem>().exitRoom = true;

        }
    }
}
