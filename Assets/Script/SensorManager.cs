﻿// Copyright (c) 2014, Tokyo University of Science All rights reserved.
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. * Neither the name of the Tokyo Univerity of Science nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Diagnostics;

[Serializable]
public class AnalogChannel
{
    public SensorManager.Analog analog;
    public SensorManager.Channels sensor;


    public AnalogChannel(SensorManager.Analog a, SensorManager.Channels s)
    {
        analog = a;
        sensor = s;
    }
}

public class SensorManager : MonoBehaviour {

    public enum AcquisitionState
    {
        Run = 0,
        NotRun = 1
    };

    public enum Analog
    {
        A1,
        A2,
        A3,
        A4,
        A5,
        A6
    }

    public enum Channels
    {
        EMG,
        EDA,
        LUX,
        ECG,
        ACC,
        BATT,
        EEG
    };

    // if need a GUI
    public GUIBitalino GUIB;
    public SensorSerialPort scriptSerialPort;
    //public AnalogChannel[] analogAndChannels = new AnalogChannel[2] { new AnalogChannel(Analog.A1, Channels.ECG), new AnalogChannel(Analog.A2, Channels.EDA) };
    private AnalogChannel[] analogAndChannels = new AnalogChannel[2] { new AnalogChannel(Analog.A1, Channels.ECG), new AnalogChannel(Analog.A2, Channels.EDA) };

    public Analog[] Analogs
    {
        get
        {
            return analogAndChannels.Select(x => x.analog).ToArray();
        }
    }

    [HideInInspector]
    public Channels[] AnalogChannels
    {
        get { return analogAndChannels.Select(x => x.sensor).ToArray(); }
    }
    public int SamplingFrequency = 100;
    public bool logFile = false;
    public string logPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

    private bool isReady = false;
    private static BITalinoDevice device;
    private IBITalinoCommunication bitalinoCommunication;
    private string version;
    private StreamWriter sw = null;
    private AcquisitionState acquisitionState = AcquisitionState.NotRun;

    // Pour updateDataSensor
    public SensorReader reader;
    private int channelECG = 0;
    private int channelEDA = 1;

    // Pour getBPM
    private float lastECGValue = -100;
    private int BPM = 80;
    private int defaultBPM = 0;
    private bool rise = true;
    private readonly Stopwatch stopWatch = new Stopwatch();
    private int cptECGValue;

    #region GETTER/SETTER
    public bool IsReady
    {
        get { return isReady; }
    }
    public AcquisitionState Acquisition_State
    {
        get { return acquisitionState; }
    }
    public IBITalinoCommunication BitalinoCommunication
    {
        get { return bitalinoCommunication; }
        set { bitalinoCommunication = value; }
    }
    #endregion

    // Renvoie les données des deux capteurs à un temps t
    public List<float> updateDataSensor()
    {
        List<float> sensorValues = new List<float>(2); // [ECG,EDA]
        if(reader.asStart)
        {
            BITalinoFrame f;
            f = reader.getBuffer()[0];
            float ECGValue = (float) f.GetAnalogValue(channelECG);
            float EDAValue = (float)f.GetAnalogValue(channelEDA);
            sensorValues.Add(ECGValue);
            sensorValues.Add(EDAValue);
        }
        return sensorValues;
    }

    // Récupère le BPM à partir de la valeur du capteur
    public int getBPM(float ECGValue)
    {
        // Initialisation
        if(lastECGValue == -100)
            lastECGValue = ECGValue;


        // Si la valeur descend, on est juste après un pic
        if (ECGValue < lastECGValue-0.05 && rise)
        {
            cptECGValue = 0;
            rise = false;

            if(stopWatch.IsRunning)
            {
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                int newBPM = (int)(60 / ts.TotalSeconds);

                if (newBPM > 50 && newBPM < 150)
                {
                    if (defaultBPM == 0)
                        defaultBPM = newBPM;

                    int gap = Math.Abs(newBPM - defaultBPM);
                    //UnityEngine.Debug.Log("gap " + gap + " newBPM " + newBPM);
                    defaultBPM = newBPM;

                    if (gap < 15)
                    {
                        BPM = newBPM;
                        UnityEngine.Debug.Log("BPM " + BPM);
                    }
                }

                stopWatch.Reset();
            }

            stopWatch.Start();
        }
        // Sinon la valeur remonte
        else
            rise = true;

        lastECGValue = ECGValue;

        return BPM;
    }

    // Use this for initialization
	void Start ()
    {
        logPath += "\\" + DateTime.Now.ToString("MMddHHmmssfff") + "_Log.txt";
        if ( GUIB != null )
        {
            GUIB.ManagerB = this;
        }
        
        if( scriptSerialPort != null )
        {
            scriptSerialPort.ManagerB = this;
        } 
	}
	
	/// <summary>
	/// Update the state of the manager
	/// </summary>
	void Update () 
    {
        if ( bitalinoCommunication != null )
        {
            isReady = true;
        }
        else if ( isReady == true )
        {
            isReady = false;
        }

        // Test pour capteurs
        if(reader.asStart)
        {
            List<float> sensorValues = updateDataSensor();
            int BPM = getBPM(sensorValues[0]);
            //UnityEngine.Debug.Log("BPM " + BPM);
        }

	}

    /// <summary>
    /// Initialize the connection with the BITalino
    /// </summary>
    public void Connection()
    {
        try
        {
            if ( bitalinoCommunication != null && device == null )
            {
                device = new BITalinoDevice(bitalinoCommunication, convertChannels(), SamplingFrequency);
            }

            device.Connection ( );
            WriteLog("Done, Connection");
        }
        catch ( Exception ex )
        {
            WriteLog("Error on connection" + ex.Message);
        }
    }

    /// <summary>
    /// Stop the connection with the BITalino
    /// </summary>
    public void Deconnection ( )
    {
        try
        {
            device.Deconnection ( );
            acquisitionState = AcquisitionState.NotRun;
            WriteLog("Done, Deconnection");
        }
        catch ( Exception ex )
        {
            WriteLog("Error on deconnection" + ex.Message);
        }

    }

    /// <summary>
    /// Get the version of the BITalino
    /// </summary>
    public void GetVersion ()
    {
        try
        {
            version = device.GetVersion ( );
            WriteLog("Bitalino's version: " + version);
        }
        catch ( Exception ex )
        {
            WriteLog("Error getting version: " + ex.Message);
        } 
    }

    /// <summary>
    /// Start the acquisition
    /// </summary>
    public void StartAcquisition()
    {
        try
        {
            device.SamplingRate = SamplingFrequency;
            device.AnalogChannels = convertChannels();
            device.StartAcquisition ();
            acquisitionState = AcquisitionState.Run;
            WriteLog("[SensorManager] Done, acquisition started");
        }
        catch ( Exception ex )
        {
            WriteLog("Error acquisition: " + ex.Message);
        }
        
    }

    /// <summary>
    /// Stop the acquisition
    /// </summary>
    public void StopAcquisition()
    {
        try
        {
            device.StopAcquisition ( );
            acquisitionState = AcquisitionState.NotRun;
            WriteLog("[SensorManager] DONE acquisition stopped");
        }
        catch ( Exception ex )
        {
            WriteLog( "Error stopping the acquisition: " + ex.Message );
        }
    }

    /// <summary>
    /// Read data from the BITalino
    /// </summary>
    /// <param name="nbSamples">number of sample reade</param>
    /// <returns>Samples read</returns>
    public BITalinoFrame [ ] Read ( int nbSamples )
    {
        try
        {
            return device.ReadFrames ( nbSamples );
        }
        catch ( Exception ex )
        {
            WriteLog( "Error reading the frames: " + ex.Message );
        }

        return null;
    }

    /// <summary>
    /// Write the log data in a file if log_file is true, else write them in the console
    /// </summary>
    /// <param name="log">Data write</param>
    public void WriteLog(String log)
    {
        if (logFile)
        {
            if (sw == null)
            {
                sw = File.AppendText(logPath);
            }
            sw.WriteLine(log);
            sw.Flush();
        }
        else
        {
            UnityEngine.Debug.Log(log);
        }
    }

    /// <summary>
    /// Convert the tab of AnalogChannels into an int tab
    /// </summary>
    /// <returns>Int tab of the AnalogChannels</returns>
    public int[] convertChannels()
    {
        int[] convertChannels = new int[Analogs.Length];
        int i = 0;
        foreach (Channels channel in Analogs)
        {
            convertChannels[i] = (int)channel;
            i++;
        }
        return convertChannels;
    }
}
