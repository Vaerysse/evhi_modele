using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using static OVRInput;

public class TPMove : MonoBehaviour
{
    public Controller touch; // la touche controller avec lequel on controle le d�placement par t�l�portation
    public GameObject controller; // Le controller qu'on utilise
    public GameObject endRendererInf; // Le controller qu'on utilise
    public LineRenderer lineRenderer; // La ligne de vision � d�essinner pour montrer la zone ou l'on touche
    public float widthLineRender; // La taille voulue de la ligne de vis�
    public GameObject TPTarget; // L'objet qui sert de rep�rer pour la vis� de la zone de t�l�portation
    public Image imgCenter; // Stock l'image du fondue
    public AnimationCurve curve; // Courbe permettant de modifier math�matiquement l'effet du fondu
    public float timeFade; // Le temps que doit durer le fondue
    public float sensibilityRotation; // La sensibilit� de la rotation entre 0 et 1
    public int degreRotation; // Le degre de rotation voulue � chaque demande de retation

    private bool moveAutorized = true; // Autorise la t�l�portation si l'on appuie sur un certain bouton
    private Vector3 TPPosition = Vector3.zero; // Les coordonn�e enregistrer pour la t�l�portation
    private bool rotationAutorized = true; // Demande de retation autoris�

    private void Start()
    {
        LineRendererSetting();
        ResetZoneMoveLook();
    }

    // Update is called once per frame
    void Update()
    {
        // Si l'on active le joystick gauche, on fait apparaitre la ligne et la zone de vis� pour la t�l�portation
        if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick) != Vector2.zero)
        {
            ZoneMoveLook();
        }
        else // sinon on d�sactive la ligne et la zone
        {
            ResetZoneMoveLook();
        }


        // Si on vise correctement et qu'on appuis sur la touche index gauche, on se t�l�porte au point vis�
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, touch) > 0f && TPPosition != Vector3.zero && moveAutorized)
        {
            TeleportUser();
        }// On doit arr�ter d'appuyer sur la touche index gauche pour pouvoir refaire une t�l�portation
        else if(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, touch) == 0f && !moveAutorized)
        {
            moveAutorized = true;
        }

        //Si l'on active le joystick droit vers la droite on tourne � droite, sinon si c'est � gauche on tourne vers la gauche
        if (OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x > sensibilityRotation && rotationAutorized)
        {
            rotationAutorized = false;
            Rotation(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x);
        }
        else if (OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x < -sensibilityRotation && rotationAutorized)
        {
            rotationAutorized = false;
            Rotation(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x);
        }
        else if(OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick).x == 0f)
        {
            rotationAutorized = true;
        }

    }

    /*
     * Effectue un fondue du noir rapide entre chaque d�placement
     * Durant le d�placement, on interdit tous nouveau d�placement
     * 
     * Entr�e : Null
     * 
     * Sortie : Null
     */
    IEnumerator TeleportUserWithFade()
    {
        moveAutorized = false;
        float t = timeFade/2;
        float coefFade = 1 / (timeFade/2);

        // On cr�er un fond en noir progressif
        while (t < timeFade / 2)
        {
            t += Time.deltaTime;
            float a = curve.Evaluate(t * coefFade);
            imgCenter.color = new Color(0f, 0f, 0f, a);

            yield return 0; // Saute une frame, la suite du code continuera � la frame suivante
        }
        transform.position = new Vector3(TPPosition.x, TPPosition.y + 0.5f, TPPosition.z);
        //On sort du fondue en noir progressivement
        while (t > 0f)
        {
            t -= Time.deltaTime;
            float a = curve.Evaluate(t * coefFade);
            imgCenter.color = new Color(0f, 0f, 0f, a);

            yield return 0; // Saute une frame, la suite du code continuera � la frame suivante
        }
    }

    /*
     * Param�tre le line renderer qui sert de ligne de vis�
     * Determine sa taille et la d�sactive au d�marrage de l'application
     */
    private void LineRendererSetting()
    {
        lineRenderer.enabled = false;
        lineRenderer.startWidth = widthLineRender;
        lineRenderer.endWidth = widthLineRender;
    }

    /*
     * Permet de montrer la zone vis�e pour un d�placement en t�l�portation
     * Si une surface n'est pas vis�, on active seulement la ligne de vis� (dans la direction vis�) sur 5 unit�
     * 
     * Entr�e : Null
     * 
     * Sortie : Null
     */
    private void ZoneMoveLook()
    {
        if (!TPTarget.activeSelf)
        {
            TPTarget.SetActive(true);
        }

        lineRenderer.enabled = true;
        Ray ray = new Ray(controller.transform.position, controller.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            Vector3 move = hit.point;
            TPTarget.transform.position = hit.point;
            lineRenderer.SetPosition(0, controller.transform.position);
            lineRenderer.SetPosition(1, hit.point);
            if (moveAutorized)
            {
                TPPosition = hit.point;
            }
        }
        else
        {
            lineRenderer.SetPosition(0, controller.transform.position);
            lineRenderer.SetPosition(1, endRendererInf.transform.position);
            TPPosition = Vector3.zero;
            TPTarget.transform.position = transform.position;
        }

    }

    /*
     * Permet de cacher le linerenderer pour la localisation du pointeur pour se t�l�porter
     * 
     * Entr�e : Null
     * 
     * Sortie : Null
     */
    private void ResetZoneMoveLook()
    {
        TPTarget.transform.position = transform.position;
        lineRenderer.enabled = false;
        TPPosition = Vector3.zero;
        TPTarget.SetActive(false);
    }

    /*
     * D�clanche la coroutine permettant la t�l�portation de l'utilisateur ainsi que le fondue en noir lors de la t�l�portation
     * 
     * Entr�e : Null
     *
     * Sortie : Null
     */
    private void TeleportUser()
    {
        StartCoroutine(TeleportUserWithFade());
    }

    /*
     * Fait effectuer une rotation selon la valeur de rotation re�ue
     * 
     * Entr�e : float rotationMove -> si n�gative, rotation vers la gauche, sinon vers la droite
     * 
     * Sortie : Null
     */
    private void Rotation(float rotationMove)
    {
        float degre = (float)degreRotation;
        Debug.Log(rotationMove);
        if(rotationMove < 0)
        {
            transform.Rotate(transform.rotation.x, transform.rotation.y - degre, transform.rotation.z, Space.Self);
        }
        else
        {
            transform.Rotate(transform.rotation.x, transform.rotation.y + degre, transform.rotation.z, Space.Self);
        }
    }
}
